# Welcome to the Git Course!

## Session 1:
---

* How does Git work?

* Centralized vs. distributed version control systems

* Use the console, Luke!

    * Cloning a repo
    * Making changes to our files
    * Adding new files and staging them
    * Create a new branch
    * Commit and push
    * Push a new branch
    * Merge and rebase, what should I use?
